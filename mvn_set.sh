#!/usr/bin/bash
set -e

# INPUT
server_id="$1"
server_username="$2"
server_password="$3"
mvn_settings_file="$4"


function check_if_exist()
{
    if [ ! -f "$1" ]; then
        settings_exists=false
    else
        settings_exists=true
    fi
}

function init_empty_settings()
{
    printf '<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <pluginGroups>
  </pluginGroups>

  <proxies>
  </proxies>

  <servers>
  </servers>

  <mirrors>
  </mirrors>

  <profiles>
  </profiles>

</settings>' > "$1"
}

function insert_server_cred()
{
    key='<\/servers>'
    content="  <server>\n      <id>$1</id>\n      <username>$2</username>\n      <password>$3</password>\n    </server>\n  </servers>"
    sed -i "s|$key|$content|g" "$4"
}




echo "INFO: Checking if settings.xml file exists..."
check_if_exist $mvn_settings_file

if [ $settings_exists = false ]; then
    echo "WARNING: settings.xml file doesn't exist"
    echo "INFO: Creating directories..."
    mvn_settings_dir=$(dirname "$mvn_settings_file")
    mkdir -p "$mvn_settings_dir"
    echo "INFO: Directories created"
    echo "INFO: Initializing empty config..."
    init_empty_settings $mvn_settings_file
    echo "INFO: Config initialized"
else
    echo "INFO: settings.xml exists"
fi

echo "INFO: Inserting credentials: [id, user, password]..."
insert_server_cred $server_id $server_username $server_password $mvn_settings_file
echo "INFO: Credentials inserted"