# OkayMusic
For detail info check [wiki](https://gitlab.com/MichalSlotwinski/okaymusic/-/wikis/Home) pages.
### Install infrastructure
```shell
docker-compose -f docker-compose.yml up -d
```

### Deploy
To deploy whole project create Run configuration which will execute multiple commands one after the other:
```shell
# cleans project builds
mvn clean
# deploys sql scripts
mvn -pl sql verify
# deploys web application
mvn -pl web tomcat7:redeploy
```