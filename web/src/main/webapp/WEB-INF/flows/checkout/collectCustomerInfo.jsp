<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<main>
  <div class="container" style="padding-top: 7rem;">
    <div class="border-bottom mb-4">
      <h1>Customer</h1>
      <p class="lead">Customer Details</p>
    </div>

    <form:form modelAttribute="order">

      <section>
        <h3>Basic Info</h3>
        <div class="mb-3">
          <label for="name"><strong>Name</strong></label> <form:errors path="cart.customer.name" cssStyle="color: red"/>
          <form:input path="cart.customer.name" id="name" cssClass="form-control"/>
        </div>
        <div class="mb-3">
          <label for="email"><strong>E-mail</strong></label> <form:errors path="cart.customer.email" cssStyle="color: red"/>
          <form:input path="cart.customer.email" id="email" cssClass="form-control"/>
        </div>
        <div class="mb-3">
          <label for="phone"><strong>Phone number</strong></label> <form:errors path="cart.customer.phone" cssStyle="color: red"/>
          <form:input path="cart.customer.phone" id="phone" cssClass="form-control"/>
        </div>
      </section>

      <section>
        <h3>Billing address</h3>
        <div class="mb-3">
          <label for="billingStreetName"><strong>Street Name</strong></label> <form:errors path="cart.customer.billingAddress.streetName" cssStyle="color: red"/>
          <form:input path="cart.customer.billingAddress.streetName" id="billingStreetName" cssClass="form-control"/>
        </div>
        <div class="mb-3">
          <label for="billingApartmentNumber"><strong>Apartment Number</strong></label> <form:errors path="cart.customer.billingAddress.apartmentNumber" cssStyle="color: red"/>
          <form:input path="cart.customer.billingAddress.apartmentNumber" id="billingApartmentNumber" cssClass="form-control"/>
        </div>
        <div class="mb-3">
          <label for="billingCountry"><strong>Country</strong></label> <form:errors path="cart.customer.billingAddress.country" cssStyle="color: red"/>
          <form:input path="cart.customer.billingAddress.country" id="billingCountry" cssClass="form-control"/>
        </div>
        <div class="mb-3">
          <label for="billingState"><strong>State</strong></label> <form:errors path="cart.customer.billingAddress.state" cssStyle="color: red"/>
          <form:input path="cart.customer.billingAddress.state" id="billingState" cssClass="form-control"/>
        </div>
        <div class="mb-3">
          <label for="billingCity"><strong>City</strong></label> <form:errors path="cart.customer.billingAddress.city" cssStyle="color: red"/>
          <form:input path="cart.customer.billingAddress.city" id="billingCity" cssClass="form-control"/>
        </div>
        <div class="mb-3">
          <label for="billingZipCode"><strong>Zip Code</strong></label> <form:errors path="cart.customer.billingAddress.zipCode" cssStyle="color: red"/>
          <form:input path="cart.customer.billingAddress.zipCode" id="billingZipCode" cssClass="form-control"/>
        </div>
      </section>

      <%--            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
      <input type="hidden" name="_flowExecutionKey"/>
      <input type="submit" value="Next" class="btn btn-primary mt-3" name="_eventId_customerInfoCollected">
      <button class="btn btn-primary mt-3" name="_eventId_cancel">Cancel</button>
    </form:form>
  </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>