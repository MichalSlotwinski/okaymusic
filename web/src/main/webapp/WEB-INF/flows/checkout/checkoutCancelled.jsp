<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<div class="container">
  <section>
    <div class="p-5 mb-4 bg-light rounded-3" style="padding-top: 7rem;">
      <h1 class="alert-danger">Checkout Cancelled</h1>
      <p>Your checkout process is cancelled. You may continue shopping.</p>
    </div>
  </section>

  <section>
    <p>
      <a href="<spring:url value="/product/productList"/>" class="btn btn-primary">Products</a>
    </p>
  </section>
</div>

<%@include file="/WEB-INF/view/template/footer.jsp"%>