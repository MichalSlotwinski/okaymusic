<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<main>
    <div class="container" style="padding-top: 7rem;">
        <div class="border-bottom mb-4">
            <h1>Customer</h1>
            <p class="lead">Customer Details</p>
        </div>

        <form:form modelAttribute="order">

            <section>
                <h3>Shipping address</h3>
                <div class="mb-3">
                    <label for="shippingStreetName"><strong>Street Name</strong></label> <form:errors path="cart.customer.shippingAddress.streetName" cssStyle="color: red"/>
                    <form:input path="cart.customer.shippingAddress.streetName" id="shippingStreetName" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingApartmentNumber"><strong>Apartment Number</strong></label> <form:errors path="cart.customer.shippingAddress.apartmentNumber" cssStyle="color: red"/>
                    <form:input path="cart.customer.shippingAddress.apartmentNumber" id="shippingApartmentNumber" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingCountry"><strong>Country</strong></label> <form:errors path="cart.customer.shippingAddress.country" cssStyle="color: red"/>
                    <form:input path="cart.customer.shippingAddress.country" id="shippingCountry" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingState"><strong>State</strong></label> <form:errors path="cart.customer.shippingAddress.state" cssStyle="color: red"/>
                    <form:input path="cart.customer.shippingAddress.state" id="shippingState" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingCity"><strong>City</strong></label> <form:errors path="cart.customer.shippingAddress.city" cssStyle="color: red"/>
                    <form:input path="cart.customer.shippingAddress.city" id="shippingCity" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingZipCode"><strong>Zip Code</strong></label> <form:errors path="cart.customer.shippingAddress.zipCode" cssStyle="color: red"/>
                    <form:input path="cart.customer.shippingAddress.zipCode" id="shippingZipCode" cssClass="form-control"/>
                </div>
            </section>

            <%--            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
            <input type="hidden" name="_flowExecutionKey"/>
            <button class="btn btn-outline-primary mt-3" name="_eventId_backToCollectCustomerInfo">Back</button>
            <input type="submit" value="Next" class="btn btn-primary mt-3" name="_eventId_shippingDetailCollected">
            <button class="btn btn-outline-primary mt-3" name="_eventId_cancel">Cancel</button>
        </form:form>
    </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>