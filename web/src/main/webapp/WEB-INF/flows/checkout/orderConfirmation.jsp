<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>
<jsp:useBean id="now" class="java.util.Date"/>

<main>
    <div class="container" style="padding-top: 7rem;">
        <div class="border-bottom mb-4">
            <h1>Order</h1>
            <p class="lead">Order Confirmation</p>
        </div>
    </div>

    <div class="container">
        <form:form modelAttribute="order">
            <div class="card" style="padding: 1.5em">
                <div class="text-center">
                    <h1>Receipt</h1>
                </div>
                <div class="row">
                    <div class="col-6 col-sm-6 col-md-6">
                        <address>
                            <strong>Shipping Address</strong>
                            <p>${order.cart.customer.shippingAddress.streetName}</p>
                            <p>
                                ${order.cart.customer.shippingAddress.apartmentNumber}, ${order.cart.customer.shippingAddress.state}
                            </p>
                            <p>
                                ${order.cart.customer.shippingAddress.country}, ${order.cart.customer.shippingAddress.zipCode}
                            </p>
                        </address>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 text-end">
                        <p>Shipping Date: <fmt:formatDate type="date" value="${now}"/></p>
                    </div>
                    <div class="row">
                        <div class="col-6 col-sm-6 col-md-6">
                            <address>
                                <strong>Billing Address</strong>
                                <p>${order.cart.customer.billingAddress.streetName}</p>
                                <p>
                                    ${order.cart.customer.billingAddress.apartmentNumber}, ${order.cart.customer.billingAddress.state}
                                </p>
                                <p>
                                    ${order.cart.customer.billingAddress.country}, ${order.cart.customer.billingAddress.zipCode}
                                </p>
                            </address>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th class="text-center">#</th>
                                <th class="text-center">Price</th>
                                <th class="text-center">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="item" items="${order.cart.cartItems}">
                            <tr>
                                <td><em>${item.product.name}</em></td>
                                <td style="text-align: center">${item.quantity}</td>
                                <td style="text-align: center">${item.product.price}</td>
                                <td style="text-align: center">${item.totalPrice}</td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td></td>
                            <td></td>
                            <td class="text-end">
                                <h4><strong>Grand Total: </strong></h4>
                            </td>
                            <td class="text-center text-danger">
                                <h4><strong>$ ${order.cart.grandTotal}</strong></h4>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
            </div>
            <input type="hidden" name="_flowExecutionKey"/>
            <button class="btn btn-outline-primary mt-3" name="_eventId_backToCollectShippingDetail">Back</button>
            <input type="submit" value="Submit Order" class="btn btn-primary mt-3" name="_eventId_orderConfirmed"/>
            <button class="btn btn-outline-primary mt-3" name="_eventId_cancel">Cancel</button>
        </form:form>
    </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>