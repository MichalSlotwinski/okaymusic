var cartApp = angular.module("cartApp", []);

cartApp.controller("cartCtrl", ["$scope", "$http", function ($scope, $http) {

    $scope.refreshCart = function () {
        $http.get("/okaymusic/rest/cart/" + $scope.cartId).then(function (data) {
            $scope.cart=data.data;
            $scope.cart.grandTotal = $scope.computeGrandTotal();
        });
    };

    $scope.clearCart = function () {
        $http.delete("/okaymusic/rest/cart/" + $scope.cartId).then(function () {
            $scope.refreshCart();
        });
    };

    $scope.initCartId = function (cartId) {
        $scope.cartId = cartId;
        $scope.refreshCart();
    };

    $scope.addToCart = function (productId) {
        $http.put("/okaymusic/rest/cart/add/" + productId).then(function () {
            alert("Product successfully added to cart !");
        });
    };

    $scope.removeFromCart = function (productId) {
        $http.put("/okaymusic/rest/cart/remove/" + productId).then(function () {
            $scope.refreshCart();
        });
    };

    $scope.computeGrandTotal = function () {
        let grandTotal=0;
        for(let i = 0; i < $scope.cart.cartItems.length; i++) {
            grandTotal += $scope.cart.cartItems[i].totalPrice;
        }
        return grandTotal;
    }
}]);