<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<main>
    <div id="myCarousel" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="myCarousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
            <button type="button" data-bs-target="myCarousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
            <button type="button" data-bs-target="myCarousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="carousel-item-color carousel-item-alignment">
                    <img src="<c:url value="/resources/img/audience.jpg"/>" class="carousel-image-scaling" aria-hidden="true" alt="Your favourite band">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Professional grade equipment</h1>
                            <p>Reliable instruments</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="carousel-item-color carousel-item-alignment">
                    <img src="<c:url value="/resources/img/headphones.jpg"/>" aria-hidden="true" class="carousel-image-scaling" alt="Audio accessories">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1 class="text-black">Audio accessories</h1>
                            <p class="text-black">For everyday use</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="carousel-item-color carousel-item-alignment">
                    <img src="<c:url value="/resources/img/piano.jpg"/>" aria-hidden="true" class="carousel-image-scaling" alt="Classical instruments">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Classical instruments</h1>
                            <p>To learn and play</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#myCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#myCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <div class="container marketing">
        <div class="row">
            <div class="col-lg-4">
                <img class="rounded-circle" src="<c:url value="/resources/img/instrument.jpg"/>" width="140" height="140" role="img" aria-label="Placeholder: 140x140" title="Instrument" alt="Instrument Image"/>
                <h2>Instrument</h2>
                <p>Well crafted and delicate instruments</p>
                <p><a class="btn btn-secondary" href="<c:url value="/product/productList?searchCondition=Instrument"/>" role="button">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <img class="rounded-circle" src="<c:url value="/resources/img/record.jpg"/>" width="140" height="140" role="img" aria-label="Placeholder: 140x140" title="Record" alt="Record Image"/>
                <h2>Discography</h2>
                <p>An exceptional collections of music records</p>
                <p><a class="btn btn-secondary" href="<c:url value="/product/productList?searchCondition=Record"/>">View details &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <img class="rounded-circle" src="<c:url value="/resources/img/accessory.jpg"/>" width="140" height="140" role="img" aria-label="Placeholder: 140x140" title="Accessories" alt="Accessory Image"/>
                <h2>Accessories</h2>
                <p>All musical and related geeky goods</p>
                <p><a class="btn btn-secondary" href="<c:url value="/product/productList?searchCondition=Accessory"/>">View details &raquo;</a></p>
            </div>
        </div>
    </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>