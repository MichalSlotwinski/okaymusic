<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<main>
    <div class="container" style="padding-top: 7rem;">
        <div class="border-bottom mb-4">
            <h1>Customer Management Page</h1>
            <p class="lead">This is the customer management page.</p>
        </div>
        <table class="table table-striped table-hover" style="table-layout: fixed">
            <thead>
            <tr class="table-success">
                <th class="text-center">Name</th>
                <th class="text-center">Email</th>
                <th class="text-center">Phone</th>
                <th class="text-center">Username</th>
                <th class="text-center">Enabled</th>
            </tr>
            </thead>
            <c:forEach items="${customers}" var="customer">
                <tr>
                    <td class="text-center">${customer.name}</td>
                    <td class="text-center">${customer.email}</td>
                    <td class="text-center">${customer.phone}</td>
                    <td class="text-center">${customer.username}</td>
                    <td class="text-center">${customer.enabled}</td>
                </tr>
            </c:forEach>
        </table>
    </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>