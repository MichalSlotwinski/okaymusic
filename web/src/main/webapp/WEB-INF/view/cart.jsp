<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<div class="container">
    <section>
        <div class="p-5 mb-4 bg-light rounded-3" style="padding-top: 7rem;">
            <h1>Cart</h1>
            <p>All the selected products in your shopping cart.</p>
        </div>
    </section>
    <section ng-app="cartApp" ng-controller="cartCtrl" ng-init="initCartId('${cartId}')">
        <div>
            <a class="btn btn-danger justify-content-md-start" ng-click="clearCart()">
                <span class="bi bi-x-circle"></span>
                Clear cart
            </a>
            <a class="btn btn-outline-success float-end" href="<spring:url value="/order/${cartId}"/>">
                <span class="bi bi-cart-fill"></span>
                Check out
            </a>
        </div>
        <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Product</th>
                <th>Unit Price</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
            </thead>
            <tr ng-repeat="item in cart.cartItems">
                <td>{{ item.product.name }}</td>
                <td>{{ item.product.price }}</td>
                <td>{{ item.quantity }}</td>
                <td>{{ item.totalPrice }}</td>
                <td>
                    <a href="#" class="badge bg-danger" ng-click="removeFromCart(item.product.id)">
                        <span class="bi bi-x-circle-fill"></span>remove
                    </a>

                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Grand Total</td>
                <td>{{ cart.grandTotal }}</td>
                <td></td>
            </tr>
        </table>
        <a href="<spring:url value="/product/productList"/>" class="btn btn-primary">Continue Shopping</a>
    </section>
</div>

<%@include file="/WEB-INF/view/template/footer.jsp"%>