<%@include file="/WEB-INF/view/template/header.jsp"%>

<main>
    <div class="container" style="padding-top: 7rem;">
        <div class="border-bottom mb-4">
            <h1>Administrator page</h1>
            <p class="lead">This is the administrator page.</p>
        </div>
        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <h2>Welcome: ${pageContext.request.userPrincipal.name} | <a href="<c:url value="/j_spring_security_logout"/>">Logout</a></h2>
        </c:if>
        <h3>
            <a href="<c:url value="/admin/productInventory"/>">Product Inventory</a>
        </h3>
        <p>Here you can view, check and modify product inventory.</p>
        <h3>
            <a href="<c:url value="/admin/customer"/>">Customer Management</a>
        </h3>
        <p>Here you can view customer information.</p>
    </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>