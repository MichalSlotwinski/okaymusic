<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<main>
    <div class="container" style="padding-top: 7rem;">
        <div class="border-bottom mb-4">
            <h1>Add Product</h1>
            <p class="lead">Fill the below information to add product.</p>
        </div>

        <form:form action="${pageContext.request.contextPath}/admin/product/addProduct" method="post" modelAttribute="product" enctype="multipart/form-data">
            <div class="mb-3">
                <label for="name"><strong>Name</strong></label> <form:errors path="name" cssStyle="color: red"/>
                <form:input path="name" id="name" cssClass="form-control"/>
            </div>

            <div class="mb-3">
                <label style="margin-right: 1rem"><strong>Category</strong></label>
                <div class="form-check form-check-inline">
                    <form:radiobutton path="category" id="category1" value="Instrument" cssClass="form-check-input"/>
                    <label class="form-check-label" for="category1">Instrument</label>
                </div>
                <div class="form-check form-check-inline">
                    <form:radiobutton path="category" id="category2" value="Record" cssClass="form-check-input"/>
                    <label class="form-check-label" for="category2">Record</label>
                </div>
                <div class="form-check form-check-inline">
                    <form:radiobutton path="category" id="category3" value="Accessory" cssClass="form-check-input"/>
                    <label class="form-check-label" for="category3">Accessory</label>
                </div>
            </div>

            <div class="mb-3">
                <label for="description"><strong>Description</strong></label>
                <form:textarea path="description" id="description" cssClass="form-control"/>
            </div>

            <div class="mb-3">
                <label for="price"><strong>Price</strong></label> <form:errors path="price" cssStyle="color: red"/>
                <form:input path="price" id="price" cssClass="form-control"/>
            </div>

            <div class="mb-3">
                <label style="margin-right: 1rem"><strong>Condition</strong></label>
                <div class="form-check form-check-inline">
                    <form:radiobutton path="condition" id="condition1" value="New" cssClass="form-check-input"/>
                    <label class="form-check-label" for="condition1">New</label>
                </div>
                <div class="form-check form-check-inline">
                    <form:radiobutton path="condition" id="condition2" value="Used" cssClass="form-check-input"/>
                    <label class="form-check-label" for="condition2">Used</label>
                </div>
            </div>

            <div class="mb-3">
                <label style="margin-right: 1rem"><strong>Status</strong></label>
                <div class="form-check form-check-inline">
                    <form:radiobutton path="status" id="status1" value="Active" cssClass="form-check-input"/>
                    <label class="form-check-label" for="status1">Active</label>
                </div>
                <div class="form-check form-check-inline">
                    <form:radiobutton path="status" id="status2" value="Inactive" cssClass="form-check-input"/>
                    <label class="form-check-label" for="status2">Inactive</label>
                </div>
            </div>

            <div class="mb-3">
                <label for="unitsInStock"><strong>Units In Stock</strong></label> <form:errors path="unitInStock" cssStyle="color: red"/>
                <form:input path="unitInStock" id="unitsInStock" cssClass="form-control"/>
            </div>

            <div class="mb-3">
                <label for="manufacturer"><strong>Manufacturer</strong></label>
                <form:input path="manufacturer" id="manufacturer" cssClass="form-control"/>
            </div>

            <div class="mb-3">
                <label for="image"><strong>Upload Picture</strong></label>
                <form:input path="image" id="image" type="file" accept=".jpg" cssClass="form-control"/>
            </div>

<%--            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>

            <input type="submit" value="Submit" class="btn btn-primary mt-3">
            <a href="<spring:url value="/admin/productInventory"/>" class="btn btn-outline-primary mt-3">Cancel</a>
        </form:form>
    </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>