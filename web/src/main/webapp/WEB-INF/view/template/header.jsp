<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Michał Słotwiński">

    <title>Okay Music</title>

    <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/bootstrap-icons.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/datatables.min.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/carousel-util.css"/>" rel="stylesheet">
    <script type="text/javascript" src="<c:url value="/resources/js/bootstrap.bundle.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/angular.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-3.6.0.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/datatables.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/controller.js"/>"></script>

    <!--    <meta name="theme-color" content="#7952b3">-->
</head>

<body>
<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="<c:url value="/"/>">OkayMusic</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="<c:url value="/"/>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/product/productList/all"/>">Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<c:url value="/about"/>">About us</a>
                    </li>
                </ul>
                <ul class="navbar-nav d-flex">
                    <c:if test="${pageContext.request.userPrincipal.name != null}">
                        <li>
                            <a class="nav-link">Welcome: ${pageContext.request.userPrincipal.name}</a>
                        </li>
                        <li>
                            <a class="nav-link" href="<c:url value="/j_spring_security_logout"/>">Logout</a>
                        </li>
                        <c:if test="${pageContext.request.userPrincipal.name != 'admin'}">
                            <li>
                                <a class="nav-link" href="<c:url value="/customer/cart"/>">Cart</a>
                            </li>
                        </c:if>
                        <c:if test="${pageContext.request.userPrincipal.name == 'admin'}">
                            <li>
                                <a class="nav-link" href="<c:url value="/admin"/>">Admin</a>
                            </li>
                        </c:if>
                    </c:if>
                    <c:if test="${pageContext.request.userPrincipal.name == null}">
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="/login"/>">Login</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<c:url value="/register"/>">Register</a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </div>
    </nav>
</header>