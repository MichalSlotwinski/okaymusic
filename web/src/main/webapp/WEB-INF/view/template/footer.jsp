<footer class="container mt-4">
    <p class="float-end"><a href="#"> Back to top</a></p>
    <p>&copy; 2020-2022 OkayMusic, Inc. &middot; <a href="#">Privacy</a>&middot;<a href="#">Terms</a></p>
</footer>
</body>
</html>
