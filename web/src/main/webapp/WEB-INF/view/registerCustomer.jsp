<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<main>
    <div class="container" style="padding-top: 7rem;">
        <div class="border-bottom mb-4">
            <h1>Register Customer</h1>
            <p class="lead">Please fill in your information below:</p>
        </div>

        <form:form action="${pageContext.request.contextPath}/register" method="post" modelAttribute="customer">

            <section>
                <h3>Basic Info</h3>
                <div class="mb-3">
                    <label for="name"><strong>Name</strong></label>
                    <form:errors path="name" cssStyle="color: red"/>
                    <form:input path="name" id="name" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="username"><strong>Username</strong></label>
                    <span style="color: red">${usernameMsg}</span>
                    <form:errors path="username" cssStyle="color: red"/>
                    <form:input path="username" id="username" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="password"><strong>Password</strong></label>
                    <form:errors path="password" cssStyle="color: red"/>
                    <form:password path="password" id="password" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="email"><strong>E-mail</strong></label>
                    <span style="color: red">${emailMsg}</span>
                    <form:errors path="email" cssStyle="color: red"/>
                    <form:input path="email" id="email" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="phone"><strong>Phone number</strong></label>
                    <form:errors path="phone" cssStyle="color: red"/>
                    <form:input path="phone" id="phone" cssClass="form-control"/>
                </div>
            </section>

            <section>
                <h3>Billing address</h3>
                <div class="mb-3">
                    <label for="billingStreetName"><strong>Street Name</strong></label>
                    <form:errors path="billingAddress.streetName" cssStyle="color: red"/>
                    <form:input path="billingAddress.streetName" id="billingStreetName" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="billingApartmentNumber"><strong>Apartment Number</strong></label>
                    <form:errors path="billingAddress.apartmentNumber" cssStyle="color: red"/>
                    <form:input path="billingAddress.apartmentNumber" id="billingApartmentNumber" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="billingCountry"><strong>Country</strong></label>
                    <form:errors path="billingAddress.country" cssStyle="color: red"/>
                    <form:input path="billingAddress.country" id="billingCountry" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="billingState"><strong>State</strong></label>
                    <form:errors path="billingAddress.state" cssStyle="color: red"/>
                    <form:input path="billingAddress.state" id="billingState" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="billingCity"><strong>City</strong></label>
                    <form:errors path="billingAddress.city" cssStyle="color: red"/>
                    <form:input path="billingAddress.city" id="billingCity" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="billingZipCode"><strong>Zip Code</strong></label>
                    <form:errors path="billingAddress.zipCode" cssStyle="color: red"/>
                    <form:input path="billingAddress.zipCode" id="billingZipCode" cssClass="form-control"/>
                </div>
            </section>

            <section>
                <h3>Shipping address</h3>
                <div class="mb-3">
                    <label for="shippingStreetName"><strong>Street Name</strong></label>
                    <form:errors path="shippingAddress.streetName" cssStyle="color: red"/>
                    <form:input path="shippingAddress.streetName" id="shippingStreetName" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingApartmentNumber"><strong>Apartment Number</strong></label>
                    <form:errors path="shippingAddress.apartmentNumber" cssStyle="color: red"/>
                    <form:input path="shippingAddress.apartmentNumber" id="shippingApartmentNumber" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingCountry"><strong>Country</strong></label>
                    <form:errors path="shippingAddress.country" cssStyle="color: red"/>
                    <form:input path="shippingAddress.country" id="shippingCountry" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingState"><strong>State</strong></label>
                    <form:errors path="shippingAddress.state" cssStyle="color: red"/>
                    <form:input path="shippingAddress.state" id="shippingState" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingCity"><strong>City</strong></label>
                    <form:errors path="shippingAddress.city" cssStyle="color: red"/>
                    <form:input path="shippingAddress.city" id="shippingCity" cssClass="form-control"/>
                </div>
                <div class="mb-3">
                    <label for="shippingZipCOde"><strong>Zip Code</strong></label>
                    <form:errors path="shippingAddress.zipCode" cssStyle="color: red"/>
                    <form:input path="shippingAddress.zipCode" id="shippingZipCOde" cssClass="form-control"/>
                </div>
            </section>

            <%--            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>

            <input type="submit" value="Submit" class="btn btn-primary mt-3">
            <a href="<spring:url value="/"/>" class="btn btn-outline-primary mt-3">Cancel</a>
        </form:form>
    </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>