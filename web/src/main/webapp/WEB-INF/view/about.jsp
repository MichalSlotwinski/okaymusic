<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<div class="container">
    <header>
        <h1>About us</h1>
        <p>We are a humble team who are passionate about helping our customers to get the best experience on music instruments
        and stuff. We are determined to provide the best products and services.</p>
    </header>
    <img src="<c:url value="/resources/img/showroom.jpg"/>" width="100%" alt="Showroom Image">
</div>

<%@include file="/WEB-INF/view/template/footer.jsp"%>