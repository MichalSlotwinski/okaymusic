<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<script>
    $(document).ready(function () {
        $('.table').DataTable({
            "lengthMenu": [
                [1, 2, 3, 5, 10, -1],
                [1, 2, 3, 4, 10, "all"]
            ]
        });
    })
</script>

<main>
    <div class="container" style="padding-top: 7rem;">
        <div class="border-bottom mb-4">
            <h1>Product Inventory Page</h1>
            <p class="lead">This is the product inventory page.</p>
        </div>
        <table class="table table-striped table-hover" style="table-layout: fixed">
            <thead>
            <tr class="table-success">
                <th style="text-align: center">Photo Thumb</th>
                <th style="text-align: center">Product Name</th>
                <th style="text-align: center">Category</th>
                <th style="text-align: center">Condition</th>
                <th style="text-align: center">Price</th>
                <th style="text-align: center"></th>
            </tr>
            </thead>
            <c:forEach items="${products}" var="product">
                <tr>
                    <td style="text-align: center">
                        <img src="<spring:url value="/images/${product.id}.jpg"/>" width="100%" height="200rem"/>
                    </td>
                    <td style="text-align: center">${product.name}</td>
                    <td style="text-align: center">${product.category}</td>
                    <td style="text-align: center">${product.condition}</td>
                    <td style="text-align: center">${product.price} PLN</td>
                    <td style="text-align: center">
                        <a href="<spring:url value="/product/viewProduct/${product.id}"/>">
                            <span class="bi bi-info-circle-fill"></span>
                        </a>
                        <a href="<spring:url value="/admin/product/deleteProduct/${product.id}"/>">
                            <span class="bi bi-x-circle-fill"></span>
                        </a>
                        <a href="<spring:url value="/admin/product/editProduct/${product.id}"/>">
                            <span class="bi bi-pencil-fill"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <a href="<spring:url value="/admin/product/addProduct"/>" class="btn btn-primary">Add Product</a>
    </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>