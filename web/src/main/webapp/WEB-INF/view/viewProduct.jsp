<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<main>
    <div class="container" style="padding-top: 7rem;">
        <div class="border-bottom mb-4">
            <h1>Product Detail</h1>
            <p class="lead">Here is the detail information of the product !</p>
        </div>
        <div class="container" ng-app="cartApp">
            <div class="row">
                <div class="col-md-5">
                    <img src="<spring:url value="/images/${product.id}.jpg"/>" alt="image" style="width: 100%; height: 300px"/>
                </div>
                <div class="col-md-5">
                    <h3>${product.name}</h3>
                    <p>${product.description}</p>
                    <p>
                        <strong>Manufacturer</strong> : ${product.manufacturer}
                    </p>
                    <p>
                        <strong>Category</strong> : ${product.category}
                    </p>
                    <p>
                        <strong>Condition</strong> : ${product.condition}
                    </p>
                    <h4>${product.price} PLN</h4>

                    <c:set var="role" scope="page" value="${param.role}"/>
                    <c:set var="url" scope="page" value="/product/productList"/>
                    <c:if test="${role='admin'}">
                        <c:set var="url" scope="page" value="/admin/productInventory"/>
                    </c:if>

                    <p ng-controller="cartCtrl" id="test">
                        <a href="<c:url value="${url}"/>" class="btn btn-primary">
                            Back
                        </a>

                        <a class="btn btn-warning btn-lg" ng-click="addToCart('${product.id}')">
                            <span class="bi bi-cart-fill"></span>
                            Order Now
                        </a>

                        <a href="<spring:url value="/customer/cart"/>" class="btn btn-primary">
                            <span class="bi bi-arrow-right-circle-fill"></span>
                            View Cart
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</main>

<%@include file="/WEB-INF/view/template/footer.jsp"%>