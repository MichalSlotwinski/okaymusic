<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="/WEB-INF/view/template/header.jsp"%>

<div class="container" style="padding-top: 7rem;">
    <div id="login-box">
        <h2>Login with username and password</h2>

        <c:if test="${not empty msg}">
            <div class="alert alert-primary">${msg}</div>
        </c:if>

        <form name="login-form" action="<spring:url value="/j_spring_security_check"/>" method="post">
            <c:if test="${not empty error}">
                <div class="alert alert-danger">${error}</div>
            </c:if>
            <div class="mb-3">
                <label for="username">User: </label>
                <input type="text" id="username" name="username" class="form-control"/>
            </div>

            <div class="mb-3">
                <label for="password">Password: </label>
                <input type="password" id="password" name="password" class="form-control"/>
            </div>

            <input type="submit" value="Login" class="btn btn-primary"/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </form>
    </div>
</div>

<%@include file="/WEB-INF/view/template/footer.jsp"%>