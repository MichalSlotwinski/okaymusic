package pl.rabbit.okaymusic.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.rabbit.okaymusic.model.Customer;
import pl.rabbit.okaymusic.model.Product;
import pl.rabbit.okaymusic.service.CustomerService;
import pl.rabbit.okaymusic.service.ProductService;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminHome
{
    @Autowired
    private ProductService productService;

    @Autowired
    private CustomerService customerService;

    @RequestMapping
    public String adminPage()
    {
        return "admin";
    }

    @RequestMapping("/productInventory")
    public String productInventory(Model model)
    {
        List<Product> products = this.productService.getProductList();
        model.addAttribute("products", products);

        return "productInventory";
    }

    @RequestMapping("/customer")
    public String customerManagement(Model model)
    {
        List<Customer> customers = this.customerService.getAllCustomers();
        model.addAttribute("customers", customers);

        return "customerManagement";
    }
}
