package pl.rabbit.okaymusic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.rabbit.okaymusic.model.BillingAddress;
import pl.rabbit.okaymusic.model.Customer;
import pl.rabbit.okaymusic.model.ShippingAddress;
import pl.rabbit.okaymusic.service.CustomerService;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegisterController
{
    @Autowired
    private CustomerService customerService;

    @RequestMapping("/register")
    public String registerCustomer(Model model)
    {
        Customer customer = new Customer();
        BillingAddress billingAddress = new BillingAddress();
        ShippingAddress shippingAddress = new ShippingAddress();

        customer.setBillingAddress(billingAddress);
        customer.setShippingAddress(shippingAddress);

        model.addAttribute("customer", customer);

        return "registerCustomer";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerCustomer(@Valid @ModelAttribute("customer") Customer customer, BindingResult result, Model model)
    {
        if(result.hasErrors())
            return "registerCustomer";

        List<Customer> customers = this.customerService.getAllCustomers();
        for(Customer cust : customers)
        {
            if(cust.getEmail().equals(customer.getEmail()))
            {
                model.addAttribute("emailMsg", "Email already exists");
                return "registerCustomer";
            }
            if(cust.getUsername().equals(customer.getUsername()))
            {
                model.addAttribute("usernameMsg", "Username already exists");
                return "registerCustomer";
            }
        }

        customer.setEnabled(true);
        this.customerService.addCustomer(customer);

        return "registerCustomerSuccess";
    }
}
