package pl.rabbit.okaymusic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.rabbit.okaymusic.model.Customer;
import pl.rabbit.okaymusic.service.CustomerService;

@Controller
@RequestMapping("/customer/cart")
public class CartController
{
    @Autowired
    private CustomerService customerService;

    @RequestMapping
    public String getCart(@AuthenticationPrincipal User activeUser)
    {
        Customer customer = this.customerService.getCustomerByUsername(activeUser.getUsername());
        int cartId = customer.getCart().getId();

        return "redirect:/customer/cart/" + cartId;
    }

    @RequestMapping("/{id}")
    public String getCart(@PathVariable(value = "id") String cartId, Model model)
    {
        model.addAttribute("cartId", cartId);

        return "cart";
    }
}
