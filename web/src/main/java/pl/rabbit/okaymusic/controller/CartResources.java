package pl.rabbit.okaymusic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.rabbit.okaymusic.model.Cart;
import pl.rabbit.okaymusic.model.CartItem;
import pl.rabbit.okaymusic.model.Customer;
import pl.rabbit.okaymusic.model.Product;
import pl.rabbit.okaymusic.service.CartItemService;
import pl.rabbit.okaymusic.service.CartService;
import pl.rabbit.okaymusic.service.CustomerService;
import pl.rabbit.okaymusic.service.ProductService;

import java.util.List;

@Controller
@RequestMapping("/rest/cart")
public class CartResources
{
    @Autowired
    private CartService cartService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartItemService cartItemService;

    @RequestMapping("/{id}")
    public @ResponseBody Cart getCartId(@PathVariable(value = "id") int cartId)
    {
        return this.cartService.getCartById(cartId);
    }

    @RequestMapping(value = "/add/{productId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void addItem(@PathVariable(value = "productId") int productId, @AuthenticationPrincipal User activeUser)
    {
        Customer customer = this.customerService.getCustomerByUsername(activeUser.getUsername());
        Cart cart  = customer.getCart();

        Product product = this.productService.getProductById(productId);

        List<CartItem> cartItems = cart.getCartItems();
        for(CartItem item : cartItems)
        {
            if(product.getId() == item.getProduct().getId())
            {
                item.setQuantity(item.getQuantity() + 1);
                item.setTotalPrice(product.getPrice() * item.getQuantity());
                this.cartItemService.addCartItem(item);
                return;
            }
        }
        CartItem cartItem = new CartItem();
        cartItem.setProduct(product);
        cartItem.setQuantity(1);
        cartItem.setTotalPrice(product.getPrice() * cartItem.getQuantity());
        cartItem.setCart(cart);
        this.cartItemService.addCartItem(cartItem);
    }

    @RequestMapping(value = "/remove/{productId}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeItem(@PathVariable(value = "productId") int productId)
    {
        CartItem cartItem = this.cartItemService.getCartItemByProductId(productId);
        this.cartItemService.removeCartItem(cartItem);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void clearCart(@PathVariable(value = "id") int cartId)
    {
        Cart cart = this.cartService.getCartById(cartId);
        this.cartItemService.removeAllCartItems(cart);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Illegal request, please verify your payload.")
    public void handleClientErrors(Exception e) {}

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Internal Server Error.")
    public void handleServerErrors(Exception e) {}
}
