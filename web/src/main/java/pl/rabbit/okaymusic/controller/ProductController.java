package pl.rabbit.okaymusic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.rabbit.okaymusic.model.Product;
import pl.rabbit.okaymusic.service.ProductService;

import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController
{
    @Autowired
    private ProductService productService;

    @RequestMapping("/productList/all")
    public String getProducts(Model model)
    {
        List<Product> products = productService.getProductList();
        model.addAttribute("products", products);

        return "productList";
    }

    @RequestMapping("/viewProduct/{id}")
    public String viewProduct(@PathVariable int id, Model model)
    {
        Product product = this.productService.getProductById(id);
        model.addAttribute("product", product);

        return "viewProduct";
    }

    @RequestMapping("/productList")
    public String getProductByCategory(@RequestParam("searchCondition") String searchCondition, Model model)
    {
        List<Product> products = productService.getProductList();
        model.addAttribute("products", products);
        model.addAttribute("searchCondition", searchCondition);

        return "productList";
    }
}
