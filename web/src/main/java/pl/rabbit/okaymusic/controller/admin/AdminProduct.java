package pl.rabbit.okaymusic.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import pl.rabbit.okaymusic.model.Product;
import pl.rabbit.okaymusic.service.ProductService;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@PropertySource("/WEB-INF/application.properties")
@Controller
@RequestMapping("/admin")
public class AdminProduct
{
    @Value("${app.content.static.path}")
    private String resource;

    @Autowired
    private ProductService productService;

    @RequestMapping("/product/addProduct")
    public String addProduct(Model model)
    {
        Product product = new Product();
        product.setCategory("Instrument");
        product.setCondition("New");
        product.setStatus("Active");

        model.addAttribute("product", product);

        return "addProduct";
    }

    @RequestMapping(value = "/product/addProduct", method = RequestMethod.POST)
    public String addProduct(@Valid @ModelAttribute("product") Product product, BindingResult result)
    {
        if(result.hasErrors())
            return "addProduct";

        this.productService.addProduct(product);

        MultipartFile image = product.getImage();
        if(!image.isEmpty())
        {
            try
            {
                String originalExtension = image.getOriginalFilename().substring(image.getOriginalFilename().lastIndexOf('.'));
                String imageFilename = product.getId() + originalExtension;

                Path path = Paths.get(this.resource, "img");
                Files.createDirectories(path);

                path = Paths.get(path.toString(), imageFilename);
                image.transferTo(path);
            }
            catch(IOException e)
            {
                throw new RuntimeException("Product image saving failed", e);
            }
        }

        return "redirect:/admin/productInventory";
    }

    @RequestMapping("/product/editProduct/{id}")
    public String editProduct(@PathVariable int id, Model model)
    {
        Product product = this.productService.getProductById(id);
        model.addAttribute("product", product);

        return "editProduct";
    }

    @RequestMapping(value = "/product/editProduct", method = RequestMethod.POST)
    public String editProduct(@Valid @ModelAttribute("product") Product product, BindingResult result)
    {
        if(result.hasErrors())
            return "editProduct";

        MultipartFile image = product.getImage();
        if(!image.isEmpty())
        {
            try
            {
                String originalExtension = image.getOriginalFilename().substring(image.getOriginalFilename().lastIndexOf('.'));
                String imageFilename = product.getId() + originalExtension;

                Path path = Paths.get(this.resource, "img");
                Files.createDirectories(path);

                path = Paths.get(path.toString(), imageFilename);
                image.transferTo(path);
            }
            catch(IOException e)
            {
                throw new RuntimeException("Product image saving failed", e);
            }
        }

        this.productService.editProduct(product);

        return "redirect:/admin/productInventory";
    }

    @RequestMapping("/product/deleteProduct/{id}")
    public String deleteProduct(@PathVariable int id)
    {
        Product product = this.productService.getProductById(id);

        MultipartFile image = product.getImage();

        if(!image.isEmpty())
        {
            String originalExtension = image.getOriginalFilename().substring(image.getOriginalFilename().lastIndexOf('.'));
            String imageFilename = product.getId() + originalExtension;

            Path path = Paths.get(this.resource, "img", imageFilename);

            if(Files.exists(path))
            {
                try { Files.delete(path); }
                catch(IOException e) { throw new RuntimeException("Product image deletion failed", e); }
            }
        }
        this.productService.deleteProduct(product);

        return "redirect:/admin/productInventory";
    }
}
