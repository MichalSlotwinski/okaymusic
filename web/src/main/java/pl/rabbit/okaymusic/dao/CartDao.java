package pl.rabbit.okaymusic.dao;

import pl.rabbit.okaymusic.model.Cart;

import java.io.IOException;

public interface CartDao
{
    Cart getCartById(int id);

    void updateCart(Cart cart);

    Cart validate(int cartId) throws IOException;
}
