package pl.rabbit.okaymusic.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.rabbit.okaymusic.dao.CustomerDao;
import pl.rabbit.okaymusic.model.Authorities;
import pl.rabbit.okaymusic.model.Cart;
import pl.rabbit.okaymusic.model.Customer;
import pl.rabbit.okaymusic.model.Users;

import java.util.List;

@Repository
@Transactional
public class CustomerDaoImpl implements CustomerDao
{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addCustomer(Customer customer)
    {
        customer.getBillingAddress().setCustomer(customer);
        customer.getShippingAddress().setCustomer(customer);
        customer.setPassword(
                this.encodePassword(customer.getPassword())
        );

        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(customer);
        session.saveOrUpdate(customer.getBillingAddress());
        session.saveOrUpdate(customer.getShippingAddress());

        Users newUser = new Users();
        newUser.setUsername(customer.getUsername());
        newUser.setPassword(customer.getPassword());
        newUser.setEnabled(customer.isEnabled());
        newUser.setCustomerId(customer.getId());

        Authorities newAuthority = new Authorities();
        newAuthority.setUsername(newUser.getUsername());
        newAuthority.setAuthority("ROLE_USER");
        session.saveOrUpdate(newUser);
        session.saveOrUpdate(newAuthority);

        Cart newCart = new Cart();
        newCart.setCustomer(customer);
        customer.setCart(newCart);
        session.saveOrUpdate(customer);
        session.saveOrUpdate(newCart);

        session.flush();
    }

    @Override
    public Customer getCustomerById(int id)
    {
        Session session = this.sessionFactory.getCurrentSession();
        Customer customer = session.get(Customer.class, id);
        session.flush();

        return customer;
    }

    @Override
    public List<Customer> getAllCustomers()
    {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("from customer");
        List<Customer> customers = query.list();
        session.flush();

        return customers;
    }

    @Override
    public Customer getCustomerByUsername(String username)
    {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("from customer where username = ?1");
        query.setParameter(1, username);

        return (Customer) query.uniqueResult();
    }

    private String encodePassword(String password)
    {
        return "{noop}" + password;
    }
}
