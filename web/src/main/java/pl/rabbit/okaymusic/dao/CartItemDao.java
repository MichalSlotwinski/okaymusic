package pl.rabbit.okaymusic.dao;

import pl.rabbit.okaymusic.model.Cart;
import pl.rabbit.okaymusic.model.CartItem;

public interface CartItemDao
{
    void addCartItem(CartItem cartItem);

    void removeCartItem(CartItem cartItem);

    void removeAllCartItems(Cart cart);

    CartItem getCartItemByProductId(int productId);
}
