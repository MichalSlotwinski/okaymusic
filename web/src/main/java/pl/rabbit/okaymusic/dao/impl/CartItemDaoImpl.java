package pl.rabbit.okaymusic.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.rabbit.okaymusic.dao.CartItemDao;
import pl.rabbit.okaymusic.model.Cart;
import pl.rabbit.okaymusic.model.CartItem;

@Repository
@Transactional
public class CartItemDaoImpl implements CartItemDao
{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addCartItem(CartItem cartItem)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(cartItem);
        session.flush();
    }

    @Override
    public void removeCartItem(CartItem cartItem)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(cartItem);
        session.flush();
    }

    @Override
    public void removeAllCartItems(Cart cart)
    {
        for(CartItem item : cart.getCartItems())
            this.removeCartItem(item);
    }

    @Override
    public CartItem getCartItemByProductId(int productId)
    {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("from cart_item where product_id = ?1");
        query.setParameter(1, productId);
        session.flush();

        return (CartItem) query.uniqueResult();
    }
}
