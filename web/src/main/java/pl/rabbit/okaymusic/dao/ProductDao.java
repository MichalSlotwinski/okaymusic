package pl.rabbit.okaymusic.dao;

import pl.rabbit.okaymusic.model.Product;

import java.util.List;

public interface ProductDao
{
    void addProduct(Product product);

    void editProduct(Product product);

    Product getProductById(int id);

    List<Product> getAllProducts();

    void deleteProduct(Product product);
}
