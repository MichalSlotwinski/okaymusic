package pl.rabbit.okaymusic.dao.impl;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import pl.rabbit.okaymusic.dao.ProductDao;
import pl.rabbit.okaymusic.model.Product;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@PropertySource("/WEB-INF/application.properties")
@Repository
@Transactional
public class ProductDaoImpl implements ProductDao
{
    @Value("${app.content.static.path}")
    private String resource;

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public void addProduct(Product product)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(product);
        session.flush();
    }

    @Override
    public void editProduct(Product product)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(product);
        session.flush();
    }

    @Override
    public Product getProductById(int id)
    {
        Session session = this.sessionFactory.getCurrentSession();
        Product product = session.get(Product.class, id);
        try
        { product.setImage(this.getFileFromDisk(id)); }
        catch(IOException e)
        { throw new RuntimeException("Error while fetching image file from application server filesystem", e); }
        finally
        { session.flush(); }

        return product;
    }

    @Override
    public List<Product> getAllProducts()
    {
        Session session = this.sessionFactory.getCurrentSession();
        Query query = session.createQuery("from product");
        List<Product> products = query.list();
        session.flush();

        return products;
    }

    @Override
    public void deleteProduct(Product product)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.delete(product);
        session.flush();
    }

    private MultipartFile getFileFromDisk(int id) throws IOException
    {
        List<Path> images = null;
        Path imageDir = Paths.get(this.resource, "img");
        try(Stream<Path> imagePaths = Files.walk(imageDir))
        {
            images = imagePaths.filter(Files::isRegularFile)
                               .filter(path ->
                                       {
                                           String imageFilenameNoExt = path.getFileName().toString().substring(0, path.getFileName().toString().lastIndexOf('.'));
                                           return imageFilenameNoExt.equals(String.valueOf(id));
                                       }
                               )
                               .collect(Collectors.toList());
        }

        if(images.isEmpty())
            return null;
        if(images.size() > 1)
            throw new IllegalArgumentException("Found multiple images for given id: " + id);

        File imageFile = images.get(0).toFile();
        FileItem fileItem = new DiskFileItem("imageFile",
                                             Files.probeContentType(imageFile.toPath()),
                                             false,
                                             imageFile.getName(),
                                             (int) imageFile.length(),
                                             imageFile.getParentFile()
        );

        try(InputStream input = new FileInputStream(imageFile);
            OutputStream output = fileItem.getOutputStream())
        { input.transferTo(output); }

        MultipartFile image = new CommonsMultipartFile(fileItem);
        return image;
    }
}
