package pl.rabbit.okaymusic.dao;

import pl.rabbit.okaymusic.model.CustomerOrder;

public interface CustomerOrderDao
{
    void addCustomerOrder(CustomerOrder customerOrder);
}
