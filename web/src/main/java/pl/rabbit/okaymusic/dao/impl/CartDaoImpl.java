package pl.rabbit.okaymusic.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.rabbit.okaymusic.dao.CartDao;
import pl.rabbit.okaymusic.model.Cart;
import pl.rabbit.okaymusic.service.CustomerOrderService;

import java.io.IOException;

@Repository
@Transactional
public class CartDaoImpl implements CartDao
{
    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private CustomerOrderService customerOrderService;

    @Override
    public Cart getCartById(int id)
    {
        Session session = this.sessionFactory.getCurrentSession();
        return (Cart) session.get(Cart.class, id);
    }

    @Override
    public void updateCart(Cart cart)
    {
        int cartId = cart.getId();
        double grandTotal = customerOrderService.getCustomerOrderGrandTotal(cartId);
        cart.setGrandTotal(grandTotal);

        Session session = this.sessionFactory.getCurrentSession();
        session.saveOrUpdate(cart);
        session.flush();
    }

    @Override
    public Cart validate(int cartId) throws IOException
    {
        Cart cart = getCartById(cartId);
        if(cart == null || cart.getCartItems().isEmpty())
        {
            throw new IOException(cartId + "");
        }
        updateCart(cart);

        return cart;
    }
}
