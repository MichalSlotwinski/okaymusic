package pl.rabbit.okaymusic.dao;

import pl.rabbit.okaymusic.model.Customer;

import java.util.List;

public interface CustomerDao
{
    void addCustomer(Customer customer);

    Customer getCustomerById(int id);

    List<Customer> getAllCustomers();

    Customer getCustomerByUsername(String username);
}
