package pl.rabbit.okaymusic.model;

import javax.persistence.*;

@Entity(name = "authorities")
public class Authorities
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String username;
    private String authority;

    public int getId()
    {
        return id;
    }

    public void setId(int authoritiesId)
    {
        this.id = authoritiesId;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getAuthority()
    {
        return authority;
    }

    public void setAuthority(String authority)
    {
        this.authority = authority;
    }
}
