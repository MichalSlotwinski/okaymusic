package pl.rabbit.okaymusic.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "cart_item")
@AttributeOverrides({
        @AttributeOverride(name = "cartItemId", column = @Column(name = "cart_item_id")),
        @AttributeOverride(name = "totalPrice", column = @Column(name = "total_price")),
})
public class CartItem implements Serializable
{
    private static final long serialVersionUID = -1024286705295942783L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cartItemId;

    @ManyToOne
    @JoinColumn(name = "cart_id")
    @JsonIgnore
    private Cart cart;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    private int quantity;
    private double totalPrice;

    public CartItem() {}

    public CartItem(Product product)
    {
        this.product = product;
        this.quantity = 1;
        this.totalPrice = product.getPrice();
    }

    public int getCartItemId() { return cartItemId; }

    public void setCartItemId(int cartItemId) { this.cartItemId = cartItemId; }

    public Cart getCart() { return cart; }

    public void setCart(Cart cart) {this.cart = cart;}

    public Product getProduct() { return product; }

    public void setProduct(Product product)
    {
        this.product = product;
        updateTotalPrice();
    }

    public int getQuantity() { return quantity; }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
        updateTotalPrice();
    }

    public double getTotalPrice() { return totalPrice; }

    public void setTotalPrice(double totalPrice) { this.totalPrice = totalPrice; }

    protected void updateTotalPrice() { this.totalPrice = this.quantity * this.product.getPrice(); }
}
