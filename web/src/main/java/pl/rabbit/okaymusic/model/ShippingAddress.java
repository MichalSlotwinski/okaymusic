package pl.rabbit.okaymusic.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "shipping_address")
@AttributeOverrides({
        @AttributeOverride(name = "streetName", column = @Column(name = "street_name")),
        @AttributeOverride(name = "apartmentNumber", column = @Column(name = "apartment_number")),
        @AttributeOverride(name = "zipCode", column = @Column(name = "zip_code"))
})
public class ShippingAddress implements Serializable
{
    private static final long serialVersionUID = 8196059477108669764L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String streetName;
    private String apartmentNumber;
    private String city;
    private String state;
    private String country;
    private String zipCode;

    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public int getId() {return id;}

    public void setId(int id) {this.id = id;}

    public String getStreetName() {return streetName;}

    public void setStreetName(String streetName) {this.streetName = streetName;}

    public String getApartmentNumber() {return apartmentNumber;}

    public void setApartmentNumber(String apartmentNumber) {this.apartmentNumber = apartmentNumber;}

    public String getCity() {return city;}

    public void setCity(String city) {this.city = city;}

    public String getState() {return state;}

    public void setState(String state) {this.state = state;}

    public String getCountry() {return country;}

    public void setCountry(String country) {this.country = country;}

    public String getZipCode() {return zipCode;}

    public void setZipCode(String zipCode) {this.zipCode = zipCode;}

    public Customer getCustomer() {return customer;}

    public void setCustomer(Customer customer) {this.customer = customer;}

    @Override
    public String toString()
    {
        return "ShippingAddress{" +
                "streetName='" + streetName + '\'' +
                ", apartmentNumber='" + apartmentNumber + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", country='" + country + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }
}
