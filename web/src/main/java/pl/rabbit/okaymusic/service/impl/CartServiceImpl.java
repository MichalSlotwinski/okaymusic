package pl.rabbit.okaymusic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rabbit.okaymusic.dao.CartDao;
import pl.rabbit.okaymusic.model.Cart;
import pl.rabbit.okaymusic.service.CartService;

@Service
public class CartServiceImpl implements CartService
{
    @Autowired
    private CartDao cartDao;

    @Override
    public Cart getCartById(int id)
    {
        return this.cartDao.getCartById(id);
    }

    @Override
    public void updateCart(Cart cart)
    {
        this.cartDao.updateCart(cart);
    }
}
