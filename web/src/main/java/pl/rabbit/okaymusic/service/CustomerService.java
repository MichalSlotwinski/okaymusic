package pl.rabbit.okaymusic.service;

import pl.rabbit.okaymusic.model.Customer;

import java.util.List;

public interface CustomerService
{
    void addCustomer(Customer customer);

    Customer getCustomerById(int id);

    List<Customer> getAllCustomers();

    Customer getCustomerByUsername(String username);
}
