package pl.rabbit.okaymusic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rabbit.okaymusic.dao.CartItemDao;
import pl.rabbit.okaymusic.model.Cart;
import pl.rabbit.okaymusic.model.CartItem;
import pl.rabbit.okaymusic.service.CartItemService;

@Service
public class CartItemServiceImpl implements CartItemService
{
    @Autowired
    private CartItemDao cartItemDao;

    @Override
    public void addCartItem(CartItem cartItem)
    {
        this.cartItemDao.addCartItem(cartItem);
    }

    @Override
    public void removeCartItem(CartItem cartItem)
    {
        this.cartItemDao.removeCartItem(cartItem);
    }

    @Override
    public void removeAllCartItems(Cart cart)
    {
        this.cartItemDao.removeAllCartItems(cart);
    }

    @Override
    public CartItem getCartItemByProductId(int productId)
    {
        return this.cartItemDao.getCartItemByProductId(productId);
    }
}
