package pl.rabbit.okaymusic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rabbit.okaymusic.dao.CustomerDao;
import pl.rabbit.okaymusic.model.Customer;
import pl.rabbit.okaymusic.service.CustomerService;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService
{
    @Autowired
    private CustomerDao customerDao;

    @Override
    public void addCustomer(Customer customer)
    {
        this.customerDao.addCustomer(customer);
    }

    @Override
    public Customer getCustomerById(int id)
    {
        return this.customerDao.getCustomerById(id);
    }

    @Override
    public List<Customer> getAllCustomers()
    {
        return this.customerDao.getAllCustomers();
    }

    @Override
    public Customer getCustomerByUsername(String username)
    {
        return this.customerDao.getCustomerByUsername(username);
    }
}
