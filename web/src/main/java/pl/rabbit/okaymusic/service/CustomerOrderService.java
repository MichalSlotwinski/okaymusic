package pl.rabbit.okaymusic.service;

import pl.rabbit.okaymusic.model.CustomerOrder;

public interface CustomerOrderService
{
    void addCustomerOrder(CustomerOrder customerOrder);

    double getCustomerOrderGrandTotal(int cartId);
}
