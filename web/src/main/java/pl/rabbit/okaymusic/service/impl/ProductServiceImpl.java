package pl.rabbit.okaymusic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rabbit.okaymusic.dao.ProductDao;
import pl.rabbit.okaymusic.model.Product;
import pl.rabbit.okaymusic.service.ProductService;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService
{
    @Autowired
    private ProductDao productDao;

    @Override
    public List<Product> getProductList()
    {
        return this.productDao.getAllProducts();
    }

    @Override
    public Product getProductById(int id)
    {
        return this.productDao.getProductById(id);
    }

    @Override
    public void addProduct(Product product)
    {
        this.productDao.addProduct(product);
    }

    @Override
    public void editProduct(Product product)
    {
        this.productDao.editProduct(product);
    }

    @Override
    public void deleteProduct(Product product)
    {
        this.productDao.deleteProduct(product);
    }
}
