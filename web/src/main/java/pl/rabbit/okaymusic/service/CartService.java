package pl.rabbit.okaymusic.service;

import pl.rabbit.okaymusic.model.Cart;

public interface CartService
{
    Cart getCartById(int id);

    void updateCart(Cart cart);
}
