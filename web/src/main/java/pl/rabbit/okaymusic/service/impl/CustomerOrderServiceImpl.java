package pl.rabbit.okaymusic.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rabbit.okaymusic.dao.CustomerOrderDao;
import pl.rabbit.okaymusic.model.Cart;
import pl.rabbit.okaymusic.model.CartItem;
import pl.rabbit.okaymusic.model.CustomerOrder;
import pl.rabbit.okaymusic.service.CartService;
import pl.rabbit.okaymusic.service.CustomerOrderService;

import java.util.List;

@Service
public class CustomerOrderServiceImpl implements CustomerOrderService
{
    @Autowired
    private CustomerOrderDao customerOrderDao;

    @Autowired
    CartService cartService;

    @Override
    public void addCustomerOrder(CustomerOrder customerOrder)
    {
        this.customerOrderDao.addCustomerOrder(customerOrder);
    }

    @Override
    public double getCustomerOrderGrandTotal(int cartId)
    {
        double grandTotal=0;
        Cart cart = this.cartService.getCartById(cartId);
        List<CartItem> cartItems = cart.getCartItems();

        for(CartItem item : cartItems)
            grandTotal += item.getTotalPrice();

        return grandTotal;
    }
}
