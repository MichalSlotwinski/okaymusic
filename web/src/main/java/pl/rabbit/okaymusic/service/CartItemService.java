package pl.rabbit.okaymusic.service;

import pl.rabbit.okaymusic.model.Cart;
import pl.rabbit.okaymusic.model.CartItem;

public interface CartItemService
{
    void addCartItem(CartItem cartItem);

    void removeCartItem(CartItem cartItem);

    void removeAllCartItems(Cart cart);

    CartItem getCartItemByProductId(int productId);
}
