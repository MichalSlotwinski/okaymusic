INSERT INTO okaymusic.users (customer_id, username, password, enabled)
SELECT 0, 'admin', '{noop}admin', true
WHERE NOT EXISTS(
    SELECT 1 FROM okaymusic.users WHERE username ='admin'
);


INSERT INTO okaymusic.authorities (username, authority)
SELECT 'admin', 'ROLE_ADMIN'
WHERE NOT EXISTS(
    SELECT 1 FROM okaymusic.authorities WHERE username = 'admin'
);
