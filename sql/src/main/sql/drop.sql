DROP TABLE IF EXISTS okaymusic.authorities CASCADE;
DROP TABLE IF EXISTS okaymusic.billing_address CASCADE;
DROP TABLE IF EXISTS okaymusic.cart CASCADE;
DROP TABLE IF EXISTS okaymusic.cart_item CASCADE;
DROP TABLE IF EXISTS okaymusic.customer CASCADE;
DROP TABLE IF EXISTS okaymusic.customer_order CASCADE;
DROP TABLE IF EXISTS okaymusic.product CASCADE;
DROP TABLE IF EXISTS okaymusic.shipping_address CASCADE;
DROP TABLE IF EXISTS okaymusic.users CASCADE;

DROP SEQUENCE IF EXISTS okaymusic.authorities_id_seq;
DROP SEQUENCE IF EXISTS okaymusic.billing_address_id_seq;
DROP SEQUENCE IF EXISTS okaymusic.cart_id_seq;
DROP SEQUENCE IF EXISTS okaymusic.cart_item_id_seq;
DROP SEQUENCE IF EXISTS okaymusic.customer_id_seq;
DROP SEQUENCE IF EXISTS okaymusic.customer_order_id_seq;
DROP SEQUENCE IF EXISTS okaymusic.product_id_seq;
DROP SEQUENCE IF EXISTS okaymusic.shipping_address_id_seq;
DROP SEQUENCE IF EXISTS okaymusic.users_id_seq;

DROP SCHEMA IF EXISTS okaymusic;
