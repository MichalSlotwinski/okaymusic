ALTER TABLE okaymusic.billing_address
    ADD CONSTRAINT FK_BILLING_ADDRESS_ON_CUSTOMER FOREIGN KEY (customer_id) REFERENCES okaymusic.customer (id);


ALTER TABLE okaymusic.cart
    ADD CONSTRAINT FK_CART_ON_CUSTOMER FOREIGN KEY (customer_id) REFERENCES okaymusic.customer (id);


ALTER TABLE okaymusic.cart_item
    ADD CONSTRAINT FK_CART_ITEM_ON_CART FOREIGN KEY (cart_id) REFERENCES okaymusic.cart (id);

ALTER TABLE okaymusic.cart_item
    ADD CONSTRAINT FK_CART_ITEM_ON_PRODUCT FOREIGN KEY (product_id) REFERENCES okaymusic.product (id);


ALTER TABLE okaymusic.customer
    ADD CONSTRAINT FK_CUSTOMER_ON_BILLING_ADDRESS FOREIGN KEY (billing_address_id) REFERENCES okaymusic.billing_address (id);

ALTER TABLE okaymusic.customer
    ADD CONSTRAINT FK_CUSTOMER_ON_CART FOREIGN KEY (cart_id) REFERENCES okaymusic.cart (id);

ALTER TABLE okaymusic.customer
    ADD CONSTRAINT FK_CUSTOMER_ON_SHIPPING_ADDRESS FOREIGN KEY (shipping_address_id) REFERENCES okaymusic.shipping_address (id);


ALTER TABLE okaymusic.customer_order
    ADD CONSTRAINT FK_CUSTOMER_ORDER_ON_BILLING_ADDRESS FOREIGN KEY (billing_address_id) REFERENCES okaymusic.billing_address (id);

ALTER TABLE okaymusic.customer_order
    ADD CONSTRAINT FK_CUSTOMER_ORDER_ON_CART FOREIGN KEY (cart_id) REFERENCES okaymusic.cart (id);

ALTER TABLE okaymusic.customer_order
    ADD CONSTRAINT FK_CUSTOMER_ORDER_ON_CUSTOMER FOREIGN KEY (customer_id) REFERENCES okaymusic.customer (id);

ALTER TABLE okaymusic.customer_order
    ADD CONSTRAINT FK_CUSTOMER_ORDER_ON_SHIPPING_ADDRESS FOREIGN KEY (shipping_address_id) REFERENCES okaymusic.shipping_address (id);


ALTER TABLE okaymusic.shipping_address
    ADD CONSTRAINT FK_SHIPPING_ADDRESS_ON_CUSTOMER FOREIGN KEY (customer_id) REFERENCES okaymusic.customer (id);